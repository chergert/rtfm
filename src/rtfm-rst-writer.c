/* rtfm-rst-writer.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "rtfm-rst-writer"

#include <errno.h>
#include <string.h>

#include "rtfm-rst-writer.h"

typedef enum
{
  STACK_ITEM_FILE,
  STACK_ITEM_DIRECTORY,
} StackItemType;

typedef struct
{
  StackItemType  type;
  gchar         *path;
  GOutputStream *stream;
} StackItem;

struct _RtfmRstWriter
{
  GObject parent;
  GQueue  stack;
};

G_DEFINE_TYPE (RtfmRstWriter, rtfm_rst_writer, G_TYPE_OBJECT)

static void
stack_item_free (StackItem *item)
{
  g_clear_pointer (&item->path, g_free);
  g_clear_object (&item->stream);
  g_slice_free (StackItem, item);
}

static void
rtfm_rst_writer_finalize (GObject *object)
{
  RtfmRstWriter *self = (RtfmRstWriter *)object;

  g_queue_clear (&self->stack);

  G_OBJECT_CLASS (rtfm_rst_writer_parent_class)->finalize (object);
}

static void
rtfm_rst_writer_class_init (RtfmRstWriterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = rtfm_rst_writer_finalize;
}

static void
rtfm_rst_writer_init (RtfmRstWriter *self)
{
  g_queue_init (&self->stack);
}

static const gchar *
rtfm_rst_writer_get_current_dir (RtfmRstWriter *self)
{
  g_assert (RTFM_IS_RST_WRITER (self));

  for (const GList *iter = self->stack.tail; iter != NULL; iter = iter->prev)
    {
      StackItem *item = iter->data;
      if (item->type == STACK_ITEM_DIRECTORY)
        return item->path;
    }

  return ".";
}

RtfmRstWriter *
rtfm_rst_writer_new (void)
{
  return g_object_new (RTFM_TYPE_RST_WRITER, NULL);
}

gboolean
rtfm_rst_writer_push_file (RtfmRstWriter  *self,
                           const gchar    *filename,
                           GError        **error)
{
  StackItem *item;
  g_autofree gchar *path = NULL;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GFile) file = NULL;

  g_assert (RTFM_IS_RST_WRITER (self));
  g_assert (filename != NULL);
  g_assert (NULL == strchr (filename, G_DIR_SEPARATOR));

  path = g_build_filename (rtfm_rst_writer_get_current_dir (self), filename, NULL);
  file = g_file_new_for_path (path);
  stream = g_file_replace (file, NULL, FALSE, G_FILE_CREATE_REPLACE_DESTINATION, NULL, error);

  if (stream == NULL)
    return FALSE;

  item = g_slice_new0 (StackItem);
  item->type = STACK_ITEM_FILE;
  item->stream = g_steal_pointer (&stream);
  item->path = g_steal_pointer (&path);

  g_queue_push_tail (&self->stack, item);

  return TRUE;
}

gboolean
rtfm_rst_writer_push_directory (RtfmRstWriter  *self,
                                const gchar    *filename,
                                GError        **error)
{
  StackItem *item;
  g_autofree gchar *path = NULL;

  g_assert (RTFM_IS_RST_WRITER (self));
  g_assert (filename != NULL);

  if (g_path_is_absolute (filename))
    path = g_strdup (filename);
  else
    path = g_build_filename (rtfm_rst_writer_get_current_dir (self), filename, NULL);

  if (!g_file_test (path, G_FILE_TEST_IS_DIR))
    {
      if (g_mkdir_with_parents (path, 0750) != 0)
        {
          g_set_error (error,
                       G_FILE_ERROR,
                       g_file_error_from_errno (errno),
                       "Failed to create directory \"%s\": %s",
                       path, g_strerror (errno));
          return FALSE;
        }
    }

  item = g_slice_new0 (StackItem);
  item->type = STACK_ITEM_DIRECTORY;
  item->path = g_steal_pointer (&path);

  g_queue_push_tail (&self->stack, item);

  return TRUE;
}

void
rtfm_rst_writer_pop_file (RtfmRstWriter *self)
{
  StackItem *cur;

  g_assert (RTFM_IS_RST_WRITER (self));

  cur = self->stack.tail->data;

  if (cur == NULL || cur->type != STACK_ITEM_FILE)
    {
      g_warning ("Cannot pop file, current stack item is not a file");
      return;
    }

  stack_item_free (g_queue_pop_tail (&self->stack));
}

void
rtfm_rst_writer_pop_directory (RtfmRstWriter *self)
{
  StackItem *cur;

  g_assert (RTFM_IS_RST_WRITER (self));

  cur = self->stack.tail->data;

  if (cur == NULL || cur->type != STACK_ITEM_DIRECTORY)
    {
      g_warning ("Cannot pop directory, current stack item is not a directory");
      return;
    }

  stack_item_free (g_queue_pop_tail (&self->stack));
}

static GOutputStream *
rtfm_rst_writer_get_current_stream (RtfmRstWriter *self)
{
  StackItem *cur;

  g_assert (RTFM_IS_RST_WRITER (self));

  cur = self->stack.tail->data;

  if (cur == NULL || cur->type != STACK_ITEM_FILE)
    {
      g_warning ("Cannot get current stream, stack item is not a file");
      return NULL;
    }

  return cur->stream;
}

static void
rtfm_rst_writer_write_all (RtfmRstWriter *self,
                           const gchar   *text)
{
  g_autoptr(GError) error = NULL;
  GOutputStream *stream;
  gsize len;
  gsize written;

  g_assert (RTFM_IS_RST_WRITER (self));

  if (text == NULL || *text == '\0')
    return;

  if (NULL == (stream = rtfm_rst_writer_get_current_stream (self)))
    return;

  len = strlen (text);
  written = 0;

  if (!g_output_stream_write_all (stream, text, len, &written, NULL, &error))
    g_warning ("Failed to write text: %s", error->message);
}

void
rtfm_rst_writer_write_line (RtfmRstWriter *self)
{
  rtfm_rst_writer_write_all (self, "\n");
}

void
rtfm_rst_writer_write (RtfmRstWriter *self,
                       const gchar   *format,
                       ...)
{
  g_autofree gchar *str = NULL;
  va_list args;

  g_assert (RTFM_IS_RST_WRITER (self));
  g_assert (format != NULL);

  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  rtfm_rst_writer_write_all (self, str);
}

static gchar *
rtfm_rst_writer_make_line (const gchar   *str,
                           RtfmRstHeader  header)
{
  GString *gstr = g_string_new (NULL);
  gchar ch;

  switch (header)
    {
      case RTFM_RST_HEADER_H1: ch = '#'; break;
      case RTFM_RST_HEADER_H2: ch = '='; break;
      case RTFM_RST_HEADER_H3: ch = '-'; break;
      case RTFM_RST_HEADER_H4: ch = '^'; break;
      default: return NULL;
    }

  for (; *str; str = g_utf8_next_char (str))
    g_string_append_c (gstr, ch);

  return g_string_free (gstr, FALSE);
}

void
rtfm_rst_writer_write_header (RtfmRstWriter *self,
                              RtfmRstHeader  header,
                              const gchar   *format,
                              ...)
{
  g_autofree gchar *str = NULL;
  g_autofree gchar *line = NULL;
  va_list args;

  g_assert (RTFM_IS_RST_WRITER (self));
  g_assert (format != NULL);

  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);

  line = rtfm_rst_writer_make_line (str, header);

  if (header == RTFM_RST_HEADER_H1)
    {
      rtfm_rst_writer_write_all (self, line);
      rtfm_rst_writer_write_line (self);
    }

  rtfm_rst_writer_write_all (self, str);
  rtfm_rst_writer_write_line (self);

  rtfm_rst_writer_write_all (self, line);
  rtfm_rst_writer_write_line (self);
}
