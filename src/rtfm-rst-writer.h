/* rtfm-rst-writer.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTFM_RST_WRITER_H
#define RTFM_RST_WRITER_H

#include "rtfm-types.h"

G_BEGIN_DECLS

typedef enum
{
  RTFM_RST_HEADER_H1,
  RTFM_RST_HEADER_H2,
  RTFM_RST_HEADER_H3,
  RTFM_RST_HEADER_H4,
} RtfmRstHeader;

RtfmRstWriter *rtfm_rst_writer_new            (void);
gboolean       rtfm_rst_writer_push_file      (RtfmRstWriter  *self,
                                               const gchar    *filename,
                                               GError        **error);
void           rtfm_rst_writer_pop_file       (RtfmRstWriter  *self);
gboolean       rtfm_rst_writer_push_directory (RtfmRstWriter  *self,
                                               const gchar    *directory,
                                               GError        **error);
void           rtfm_rst_writer_pop_directory  (RtfmRstWriter  *self);
void           rtfm_rst_writer_write_line     (RtfmRstWriter  *self);
void           rtfm_rst_writer_write          (RtfmRstWriter  *self,
                                               const gchar    *format,
                                               ...) G_GNUC_PRINTF (2, 3);
void           rtfm_rst_writer_write_header   (RtfmRstWriter  *self,
                                               RtfmRstHeader   header,
                                               const gchar    *format,
                                               ...) G_GNUC_PRINTF (3, 4);

G_END_DECLS

#endif /* RTFM_RST_WRITER_H */
