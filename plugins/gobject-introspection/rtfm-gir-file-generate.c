/* rtfm-gir-file-generate.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "rtfm-gir-file-generate"

#include <string.h>

#include "rtfm-gir-file.h"
#include "rtfm-gir-parser.h"
#include "rtfm-gir-util.h"

#include "rtfm-gir-array.h"
#include "rtfm-gir-bitfield.h"
#include "rtfm-gir-class.h"
#include "rtfm-gir-constructor.h"
#include "rtfm-gir-doc.h"
#include "rtfm-gir-doc-deprecated.h"
#include "rtfm-gir-enumeration.h"
#include "rtfm-gir-function.h"
#include "rtfm-gir-include.h"
#include "rtfm-gir-instance-parameter.h"
#include "rtfm-gir-method.h"
#include "rtfm-gir-namespace.h"
#include "rtfm-gir-parameter.h"
#include "rtfm-gir-parameters.h"
#include "rtfm-gir-property.h"
#include "rtfm-gir-record.h"
#include "rtfm-gir-return-value.h"
#include "rtfm-gir-type.h"
#include "rtfm-gir-virtual-method.h"

typedef enum
{
  GTK_DOC_TOKEN_NONE       = 0,
  GTK_DOC_TOKEN_VAR        = 1,
  GTK_DOC_TOKEN_FUNC       = 2,
  GTK_DOC_TOKEN_MACRO      = 3,
  GTK_DOC_TOKEN_SPACE      = 4,
  GTK_DOC_TOKEN_TYPE       = 5,
  GTK_DOC_TOKEN_CODE       = 6,
  GTK_DOC_TOKEN_SYMBOL     = 7,
  GTK_DOC_TOKEN_MD_TITLE   = 8,
} GtkDocTokenType;

#define CHILDREN_OF(p,t) \
  rtfm_gir_parser_object_get_children_typed(RTFM_GIR_PARSER_OBJECT(p),t)

static gboolean
str_isspace (const gchar *str)
{
  for (; *str; str = g_utf8_next_char (str))
    if (!g_unichar_isspace (g_utf8_get_char (str)))
      return FALSE;
  return TRUE;
}

static gboolean
str_isdirective (const gchar *str)
{
  return g_str_has_prefix (str, "..") ||
         g_str_has_prefix (str, "   :");
}

static gpointer
first_child_typed (gpointer instance,
                   GType type)
{
  g_autoptr(GPtrArray) children = CHILDREN_OF (instance, type);
  if (children->len)
    return g_object_ref (g_ptr_array_index (children, 0));
  return NULL;
}

static inline gboolean
str_empty0 (const gchar *str)
{
  return !str || !*str;
}

static void
write_parameter_c (RtfmRstWriter *writer,
                   const gchar   *c_type,
                   const gchar   *name)
{
  /* TODO: Cleanup alignment of params */
  rtfm_rst_writer_write (writer, "%s %s", c_type, name);
}

static gchar *
cleanup_code_c (const gchar *input)
{
  GString *str = g_string_new (NULL);
  g_autofree gchar *lang = NULL;
  const gchar *tmp;

  if ((tmp = strstr (input, "<!-- language=\"")) ||
      (tmp = strstr (input, "<!-- Language=\"")))
    {
      const gchar *endptr;

      g_string_append_len (str, input, tmp - input);

      input = tmp + strlen ("<!-- language=\"");

      if ((endptr = strchr (input, '"')))
        {
          lang = g_strndup (input, endptr - input);
          input = strstr (input, "-->");
          if (input)
            input += strlen ("-->");
        }

      if (input)
        g_string_append (str, input);
    }
  else
    g_string_append (str, input);

  return g_string_free (str, FALSE);
}

static gchar *
gtk_doc_next_token (const gchar     **ptr,
                    GtkDocTokenType  *type)
{
  const gchar *iter = *ptr;
  const gchar *begin = *ptr;
  const gchar *tmp;
  gunichar ch;
  gchar *ret = NULL;

  *type = 0;

  if (*iter == '\0')
    return NULL;

  ch = g_utf8_get_char (iter);

  while (g_unichar_isspace (ch))
    {
      /* read up to next non-white-space */
      iter = g_utf8_next_char (iter);
      ch = g_utf8_get_char (iter);
    }

  /* we read some wite space, return it */
  if (iter != begin)
    {
      *ptr = iter;
      *type = GTK_DOC_TOKEN_SPACE;
      return g_strndup (begin, iter - begin);
    }

  /* Special case to eat * as a single token */
  if (*iter == '*')
    {
      *type = GTK_DOC_TOKEN_NONE;
      *ptr = ++iter;
      return g_strdup ("*");
    }

  /* If we see |[, then we are starting a code
   * example block, and we need to consume until
   * we find the matching ]|
   */
  if (g_str_has_prefix (iter, "|[") && (tmp = strstr (iter, "]|")))
    {
      g_autofree gchar *unformat = NULL;

      *type = GTK_DOC_TOKEN_CODE;
      *ptr = &tmp[2];
      iter += 2;
      unformat = g_strndup (iter, tmp - iter);
      return cleanup_code_c (unformat);
    }

  /* sometimes we have markdown titles */
  if (g_str_has_prefix (iter, "# "))
    {
      const gchar *endptr = strchr (iter, '\n');

      if (endptr)
        {
          iter += 2;
          *ptr = endptr;
          *type = GTK_DOC_TOKEN_MD_TITLE;
          return g_strstrip (g_strndup (iter, endptr - iter));
        }
    }

  /* Read up to whitespace for next word */
  while (ch && !g_unichar_isspace (ch))
    {
      /* if @... gets used, so swallow if necessary */
      if (strncmp (iter, "@...", 4) == 0)
        {
          iter += 4;
          break;
        }

      /* read up to next non-white-space */
      iter = g_utf8_next_char (iter);
      ch = g_utf8_get_char (iter);

      /* special case periods as they can
       * mess up our has_suffix("()") checks
       */
      switch (ch)
        {
        case '.':
        case ',':
        case ';':
        case '*':
        case '-':
        case '%':
        case '\\':
        case '`':
        case '\'':
        case '"':
          goto skip;

        /* don't walk past end of first ) in foo()) */
        case ')':
          iter++;
          goto skip;

        default: ;
        }
    }

skip:
  g_assert (begin != iter);

  ret = g_strndup (begin, iter - begin);
  *ptr = iter;
  *type = GTK_DOC_TOKEN_NONE;

  if (*ret == '#')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_TYPE;
    }
  else if (*ret == '%')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_SYMBOL;
    }
  else if (*ret == '@')
    {
      *ret = ' ';
      g_strchug (ret);
      *type = GTK_DOC_TOKEN_VAR;
    }
  else if (g_str_has_suffix (ret, "()"))
    {
      if (g_unichar_isupper (g_utf8_get_char (ret)))
        *type = GTK_DOC_TOKEN_MACRO;
      else
        *type = GTK_DOC_TOKEN_FUNC;
    }

  return ret;
}

static gchar *
linkify_text_body_c (const gchar *text)
{
  GtkDocTokenType type;
  gchar *token;
  GString *str;

  g_assert (text != NULL);

  str = g_string_new (NULL);

  for (token = gtk_doc_next_token (&text, &type);
       token != NULL;
       token = gtk_doc_next_token (&text, &type))
    {
      switch (type)
        {
        case GTK_DOC_TOKEN_VAR:
          g_string_append_printf (str, ":c:data:`%s`", token);
          break;

        case GTK_DOC_TOKEN_FUNC:
          g_string_append_printf (str, ":c:func:`%s`", token);
          break;

        case GTK_DOC_TOKEN_MACRO:
          g_string_append_printf (str, ":c:macro:`%s`", token);
          break;

        case GTK_DOC_TOKEN_SYMBOL:
          g_string_append_printf (str, ":c:type:`%s`", token);
          break;

        case GTK_DOC_TOKEN_TYPE:
          if (g_str_equal (token, "TRUE") ||
              g_str_equal (token, "FALSE") ||
              g_str_equal (token, "NULL") ||
              g_str_equal (token, "MIN") ||
              g_str_equal (token, "MAX"))
            g_string_append_printf (str, ":c:macro:`%s`", token);
          else
            g_string_append_printf (str, ":c:type:`%s`", token);
          break;

        case GTK_DOC_TOKEN_CODE:
          g_string_append_printf (str, "\n.. code-block:: c\n");
          g_string_append (str, "   :linenos:\n\n");
          g_string_append (str, token);
          g_string_append (str, "..\n");
          break;

        case GTK_DOC_TOKEN_MD_TITLE:
          g_string_append_printf (str, "**%s**\n", token);
          break;

        case GTK_DOC_TOKEN_NONE:
          if (*token == '*' || *token == '`' || *token == '\\')
            g_string_append_printf (str, "\\%s ", token);
          else
            g_string_append (str, token);
          break;

        case GTK_DOC_TOKEN_SPACE:
        default:
          /* If we have space after a newline, strip it */
          if (strchr (token, '\n'))
            {
              const gchar *iter = token;
              while ((iter = strchr (iter, '\n')))
                {
                  g_string_append (str, "\n");
                  iter++;
                }
            }
          else
            g_string_append (str, token);
          break;
        }

      g_free (token);
    }

  return g_string_free (str, FALSE);
}

/*
 * Gets the inner text of the doc, and removes newlines so
 * it can be used for parameters or return value text.
 */
static gchar *
make_shortdoc (const gchar *text)
{
  gchar *linkify;

  if (text == NULL)
    return g_strdup ("");

  linkify = linkify_text_body_c (text);

  g_strdelimit (linkify, "\"", '\'');
  g_strdelimit (linkify, "\n", ' ');

  return g_strstrip (linkify);
}

static gboolean
should_skip_params (gpointer   func,
                    GPtrArray *ar)
{
  g_autofree gchar *throw = NULL;

  g_object_get (func, "throws", &throw, NULL);

  if (throw)
    return FALSE;

  for (guint i = 0; i < ar->len; i++)
    {
      gpointer p = g_ptr_array_index (ar, i);
      if (RTFM_GIR_IS_PARAMETER (p) || RTFM_GIR_IS_INSTANCE_PARAMETER (p))
        return FALSE;
    }

  return TRUE;
}

static void
write_text_body_c (RtfmRstWriter *writer,
                   const gchar   *text)
{
  g_autofree gchar *linked = NULL;
  g_auto(GStrv) lines = NULL;

  g_assert (RTFM_RST_WRITER (writer));

  if (!text || !*text)
    return;

  linked = linkify_text_body_c (text);
  lines = g_strsplit (linked, "\n", 0);

  for (guint i = 0; lines[i]; i++)
    {
      if (str_isdirective (lines[i]))
        rtfm_rst_writer_write (writer, "%s\n", lines[i]);
      else if (str_isspace (lines[i]))
        rtfm_rst_writer_write_line (writer);
      else
        rtfm_rst_writer_write (writer, "   %s\n", lines[i]);
    }

  rtfm_rst_writer_write_line (writer);
}

static void
rtfm_gir_file_generate_func_desc_c (RtfmGirFile   *self,
                                    RtfmRstWriter *writer,
                                    gpointer       func)
{
  g_autoptr(RtfmGirReturnValue) retval = NULL;
  g_autoptr(RtfmGirArray) retar = NULL;
  g_autoptr(RtfmGirType) rettype = NULL;
  const gchar *c_type = "void";
  g_autofree gchar *name = NULL;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (RTFM_GIR_IS_FUNCTION (func) ||
            RTFM_GIR_IS_CONSTRUCTOR (func) ||
            RTFM_GIR_IS_METHOD (func) ||
            RTFM_GIR_IS_VIRTUAL_METHOD (func));

  if ((retval = first_child_typed (func, RTFM_GIR_TYPE_RETURN_VALUE)))
    {
      if ((rettype = first_child_typed (retval, RTFM_GIR_TYPE_TYPE)))
        c_type = rtfm_gir_type_get_c_type (rettype);
      else if ((retar = first_child_typed (retval, RTFM_GIR_TYPE_ARRAY)))
        c_type = rtfm_gir_array_get_c_type (retar);
    }

  if (RTFM_GIR_IS_VIRTUAL_METHOD (func))
    {
      gpointer parent = rtfm_gir_parser_object_get_parent (func);
      const gchar *klass_name = "";

      if (RTFM_GIR_IS_CLASS (parent))
        klass_name = rtfm_gir_class_get_c_type (parent);

      g_object_get (func, "name", &name, NULL);
      rtfm_rst_writer_write (writer,
                             "        \":c:type:`%s`\",\":c:func:`%s.%s`\"\n",
                             c_type, klass_name, name);
    }
  else
    {
      g_object_get (func, "c-identifier", &name, NULL);
      rtfm_rst_writer_write (writer,
                             "        \":c:type:`%s`\",\":c:func:`%s`\"\n",
                             c_type, name);
    }

}

static void
rtfm_gir_file_generate_func_c (RtfmGirFile         *self,
                               RtfmRstWriter       *writer,
                               RtfmGirParserObject *func)
{
  g_autoptr(RtfmGirReturnValue) retval = NULL;
  g_autoptr(RtfmGirArray) retar = NULL;
  g_autoptr(RtfmGirType) rettype = NULL;
  g_autoptr(RtfmGirParameters) params = NULL;
  g_autoptr(RtfmGirDoc) doc = NULL;
  g_autofree gchar *deprecated = NULL;
  g_autofree gchar *deprecated_version = NULL;
  g_autofree gchar *throws = NULL;
  g_autofree gchar *version = NULL;
  const gchar *c_type = "void";
  const gchar *c_ident = NULL;
  guint pcount = 0;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));

  if (RTFM_GIR_IS_FUNCTION (func))
    c_ident = rtfm_gir_function_get_c_identifier (RTFM_GIR_FUNCTION (func));
  else if (RTFM_GIR_IS_METHOD (func))
    c_ident = rtfm_gir_method_get_c_identifier (RTFM_GIR_METHOD (func));
  else if (RTFM_GIR_IS_VIRTUAL_METHOD (func))
    c_ident = rtfm_gir_virtual_method_get_name (RTFM_GIR_VIRTUAL_METHOD (func));
  else if (RTFM_GIR_IS_CONSTRUCTOR (func))
    c_ident = rtfm_gir_constructor_get_c_identifier (RTFM_GIR_CONSTRUCTOR (func));
  else
    g_assert_not_reached ();

  g_object_get (func,
                "deprecated", &deprecated,
                "deprecated-version", &deprecated_version,
                "throws", &throws,
                "version", &version,
                NULL);

  if ((retval = first_child_typed (func, RTFM_GIR_TYPE_RETURN_VALUE)))
    {
      if ((rettype = first_child_typed (retval, RTFM_GIR_TYPE_TYPE)))
        c_type = rtfm_gir_type_get_c_type (rettype);
      else if ((retar = first_child_typed (retval, RTFM_GIR_TYPE_ARRAY)))
        c_type = rtfm_gir_array_get_c_type (retar);
    }

  if (RTFM_GIR_IS_VIRTUAL_METHOD (func))
    {
      gpointer parent = rtfm_gir_parser_object_get_parent (func);
      const gchar *klass_name = rtfm_gir_class_get_c_type (parent);

      rtfm_rst_writer_write (writer, ".. c:function:: %s %s.%s (", c_type, klass_name, c_ident);
    }
  else
    rtfm_rst_writer_write (writer, ".. c:function:: %s %s (", c_type, c_ident);

  params = first_child_typed (func, RTFM_GIR_TYPE_PARAMETERS);

  if (params != NULL)
    {
      GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (params));

      for (guint i = 0; i < children->len; i++)
        {
          RtfmGirParserObject *param = g_ptr_array_index (children, i);

          if (!RTFM_GIR_IS_PARAMETER (param) && !RTFM_GIR_IS_INSTANCE_PARAMETER (param))
            continue;

          {
            g_autoptr(RtfmGirArray) arr = first_child_typed (param, RTFM_GIR_TYPE_ARRAY);
            g_autoptr(RtfmGirType) type = first_child_typed (param, RTFM_GIR_TYPE_TYPE);
            g_autoptr(RtfmGirVarargs) varargs = first_child_typed (param, RTFM_GIR_TYPE_VARARGS);
            const gchar *pname;

            if (RTFM_GIR_IS_PARAMETER (param))
              pname = rtfm_gir_parameter_get_name (RTFM_GIR_PARAMETER (param));
            else if (RTFM_GIR_IS_INSTANCE_PARAMETER (param))
              pname = rtfm_gir_instance_parameter_get_name (RTFM_GIR_INSTANCE_PARAMETER (param));
            else
              g_assert_not_reached ();

            if (type != NULL)
              write_parameter_c (writer, rtfm_gir_type_get_c_type (type), pname);
            else if (arr != NULL)
              write_parameter_c (writer, rtfm_gir_array_get_c_type (arr), pname);
            else if (varargs != NULL)
              rtfm_rst_writer_write (writer, "...");
            else
              g_assert_not_reached ();

            if (i + 1 < children->len)
              rtfm_rst_writer_write (writer, ", ");

            pcount++;
          }
        }
    }

  if (throws)
    {
      if (pcount)
        rtfm_rst_writer_write (writer, ", ");
      rtfm_rst_writer_write (writer, "GError **error");
    }

  rtfm_rst_writer_write (writer, ")\n");
  rtfm_rst_writer_write_line (writer);


  if (RTFM_GIR_IS_VIRTUAL_METHOD (func))
    rtfm_rst_writer_write (writer, "   :annotation: virtual\n");

  doc = first_child_typed (func, RTFM_GIR_TYPE_DOC);
  if (doc != NULL)
    write_text_body_c (writer, rtfm_gir_doc_get_inner_text (doc));

  if (params != NULL)
    {
      GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (params));

      if (should_skip_params (func, children))
        goto skip_params;

      for (guint i = 0; i < children->len; i++)
        {
          RtfmGirParserObject *param = g_ptr_array_index (children, i);

          if (!RTFM_GIR_IS_PARAMETER (param) && !RTFM_GIR_IS_INSTANCE_PARAMETER (param))
            continue;

          {
            g_autoptr(RtfmGirArray) arr = first_child_typed (param, RTFM_GIR_TYPE_ARRAY);
            g_autoptr(RtfmGirType) type = first_child_typed (param, RTFM_GIR_TYPE_TYPE);
            g_autoptr(RtfmGirVarargs) varargs = first_child_typed (param, RTFM_GIR_TYPE_VARARGS);
            g_autoptr(RtfmGirDoc) pdoc = first_child_typed (param, RTFM_GIR_TYPE_DOC);
            g_autofree gchar *ptext = make_shortdoc (pdoc ? rtfm_gir_doc_get_inner_text (pdoc) : NULL);
            const gchar *pname;
            const gchar *child_c_type = NULL;

            if (RTFM_GIR_IS_PARAMETER (param))
              pname = rtfm_gir_parameter_get_name (RTFM_GIR_PARAMETER (param));
            else if (RTFM_GIR_IS_INSTANCE_PARAMETER (param))
              pname = rtfm_gir_instance_parameter_get_name (RTFM_GIR_INSTANCE_PARAMETER (param));
            else
              g_assert_not_reached ();

            if (g_str_equal (pname, "...") && str_empty0 (ptext))
              continue;

            rtfm_rst_writer_write (writer, "   :param %s:\n", pname);
            if (!str_empty0 (ptext))
              rtfm_rst_writer_write (writer, "      %s\n", ptext);

            if (type != NULL)
              child_c_type = rtfm_gir_type_get_c_type (type);
            else if (arr != NULL)
              child_c_type = rtfm_gir_array_get_c_type (arr);

            if (child_c_type != NULL)
              rtfm_rst_writer_write (writer, "   :type %s: :c:type:`%s`\n", pname, child_c_type);

            rtfm_rst_writer_write_line (writer);
          }
        }

      if (throws)
        {
          rtfm_rst_writer_write (writer, "   :param error:\n");
          rtfm_rst_writer_write (writer, "      A location for a :c:type:`GError*`, or :c:macro:`NULL`.\n");
          rtfm_rst_writer_write (writer, "   :type error: :c:type:`GError`\n");
          rtfm_rst_writer_write_line (writer);
        }

      rtfm_rst_writer_write_line (writer);
    }

skip_params:

  if (!g_str_equal (c_type, "void"))
    {
      g_autoptr(RtfmGirDoc) retdoc = first_child_typed (retval, RTFM_GIR_TYPE_DOC);
      g_autofree gchar *rettext = make_shortdoc (retdoc ? rtfm_gir_doc_get_inner_text (retdoc) : NULL);

      if (!str_empty0 (rettext))
        {
          rtfm_rst_writer_write (writer, "   :returns:\n");
          rtfm_rst_writer_write (writer, "      %s\n", rettext);
          rtfm_rst_writer_write_line (writer);
        }

      rtfm_rst_writer_write (writer, "   :rtype: :c:type:`%s`\n", c_type);
      rtfm_rst_writer_write_line (writer);
    }

  if (!str_empty0 (version))
    {
      rtfm_rst_writer_write (writer, "   .. versionadded:: %s\n", version);
      rtfm_rst_writer_write_line (writer);
    }

  if (!str_empty0 (deprecated))
    {
      g_autoptr(RtfmGirDocDeprecated) depdoc = first_child_typed (func, RTFM_GIR_TYPE_DOC_DEPRECATED);
      g_autofree gchar *deptext = make_shortdoc (depdoc ? rtfm_gir_doc_deprecated_get_inner_text (depdoc) : NULL);

      rtfm_rst_writer_write (writer, "   .. deprecated:: %s\n", deprecated_version ?: "");
      if (deptext)
        rtfm_rst_writer_write (writer, "      %s\n", deptext);
      rtfm_rst_writer_write_line (writer);
    }

  rtfm_rst_writer_write_line (writer);
}

static gint
compare_by_prop (gconstpointer a,
                 gconstpointer b,
                 gpointer      prop_name)
{
  GObject *obj_a = *(GObject **)a;
  GObject *obj_b = *(GObject **)b;
  g_autofree gchar *value_a = NULL;
  g_autofree gchar *value_b = NULL;
  const gchar *cmp_a;
  const gchar *cmp_b;

  g_object_get (obj_a, prop_name, &value_a, NULL);
  g_object_get (obj_b, prop_name, &value_b, NULL);

  cmp_a = value_a;
  cmp_b = value_b;

  /* We want to sort things by natural names like "foo", not "get_foo" */
  if (cmp_a && (g_str_has_prefix (cmp_a, "get_") || g_str_has_prefix (cmp_a, "set_")))
    cmp_a += 4;
  if (cmp_b && (g_str_has_prefix (cmp_b, "get_") || g_str_has_prefix (cmp_b, "set_")))
    cmp_b += 4;

  return g_strcmp0 (cmp_a, cmp_b);
}

static RtfmGirClass *
find_parent (RtfmGirFile  *self,
             RtfmGirClass *klass)
{
  RtfmGirRepository *repo = rtfm_gir_file_get_repository (self);
  g_autoptr(RtfmGirNamespace) ns = first_child_typed (repo, RTFM_GIR_TYPE_NAMESPACE);
  g_autoptr(GPtrArray) classes = CHILDREN_OF (ns, RTFM_GIR_TYPE_CLASS);
  const gchar *parent = rtfm_gir_class_get_parent (klass);

  for (guint i = 0; i < classes->len; i++)
    {
      RtfmGirClass *other = g_ptr_array_index (classes, i);
      const gchar *name = rtfm_gir_class_get_name (other);

      if (g_strcmp0 (name, parent) == 0)
        return g_object_ref (other);
    }

  return NULL;
}

static GPtrArray *
find_parents (RtfmGirFile  *self,
              RtfmGirClass *klass)
{
  GPtrArray *ar = g_ptr_array_new_with_free_func (g_object_unref);

  while ((klass = find_parent (self, klass)))
    g_ptr_array_add (ar, klass); /* steals ref */

  return ar;
}

static GPtrArray *
find_subclasses (RtfmGirFile  *self,
                 RtfmGirClass *klass)
{
  GPtrArray *ar = g_ptr_array_new_with_free_func (g_object_unref);
  RtfmGirRepository *repo = rtfm_gir_file_get_repository (self);
  g_autoptr(RtfmGirNamespace) ns = first_child_typed (repo, RTFM_GIR_TYPE_NAMESPACE);
  g_autoptr(GPtrArray) classes = CHILDREN_OF (ns, RTFM_GIR_TYPE_CLASS);
  const gchar *name = rtfm_gir_class_get_name (klass);

  for (guint i = 0; i < classes->len; i++)
    {
      RtfmGirClass *other = g_ptr_array_index (classes, i);
      const gchar *parent = rtfm_gir_class_get_parent (other);

      if (g_strcmp0 (name, parent) == 0)
        g_ptr_array_add (ar, g_object_ref (other));
    }

  return ar;
}

static void
write_subclasses (RtfmGirFile   *self,
                  RtfmRstWriter *writer,
                  RtfmGirClass  *klass)
{
  g_autoptr(GPtrArray) subclasses = find_subclasses (self, klass);

  if (subclasses->len == 0)
    return;

  rtfm_rst_writer_write (writer, ":Subclasses: ");

  for (guint i = 0; i < subclasses->len; i++)
    {
      RtfmGirClass *subclass = g_ptr_array_index (subclasses, i);

      if (i != 0)
        rtfm_rst_writer_write (writer, ", ");
      rtfm_rst_writer_write (writer, ":c:type:`%s`", rtfm_gir_class_get_c_type (subclass));
    }

  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);
}

static guint
count_methods (RtfmGirClass *klass)
{
  GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (klass));
  guint count = 0;

  for (guint i = 0; i < children->len; i++)
    {
      gpointer instance = g_ptr_array_index (children, i);

      if (RTFM_GIR_IS_METHOD (instance) || RTFM_GIR_IS_VIRTUAL_METHOD (instance) || RTFM_GIR_IS_FUNCTION (instance))
        count++;
    }

  return count;
}

static GPtrArray *
find_related_records (RtfmGirFile  *self,
                      RtfmGirClass *klass)
{
  GPtrArray *ar = g_ptr_array_new_with_free_func (g_object_unref);
  RtfmGirRepository *repo = rtfm_gir_file_get_repository (self);
  RtfmGirNamespace *ns = first_child_typed (repo, RTFM_GIR_TYPE_NAMESPACE);
  GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (ns));
  const gchar *glib_type_struct = rtfm_gir_class_get_glib_type_struct (klass);

  for (guint i = 0; i < children->len; i++)
    {
      gpointer instance = g_ptr_array_index (children, i);

      if (RTFM_GIR_IS_RECORD (instance))
        {
          if (g_strcmp0 (glib_type_struct, rtfm_gir_record_get_name (instance)) == 0)
            g_ptr_array_add (ar, g_object_ref (instance));
        }
    }

  return ar;
}


static void
write_methods_preview (RtfmGirFile   *self,
                       RtfmRstWriter *writer,
                       RtfmGirClass  *klass)
{
  g_autoptr(GPtrArray) ctors = NULL;
  g_autoptr(GPtrArray) methods = NULL;
  g_autoptr(GPtrArray) funcs = NULL;
  g_autoptr(GPtrArray) virtuals = NULL;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (RTFM_GIR_IS_CLASS (klass));

  ctors = CHILDREN_OF (klass, RTFM_GIR_TYPE_CONSTRUCTOR);
  methods = CHILDREN_OF (klass, RTFM_GIR_TYPE_METHOD);
  funcs = CHILDREN_OF (klass, RTFM_GIR_TYPE_FUNCTION);
  virtuals = CHILDREN_OF (klass, RTFM_GIR_TYPE_VIRTUAL_METHOD);

  g_ptr_array_sort_with_data (methods, compare_by_prop, "name");
  g_ptr_array_sort_with_data (funcs, compare_by_prop, "name");
  g_ptr_array_sort_with_data (virtuals, compare_by_prop, "name");

  if (ctors->len || methods->len || funcs->len)
    {
      g_autoptr(GPtrArray) parents = find_parents (self, klass);

      rtfm_rst_writer_write (writer, ".. _%s.methods:\n", rtfm_gir_class_get_c_type (klass));
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Methods");
      rtfm_rst_writer_write_line (writer);

      if (parents->len)
        {
          g_autoptr(GPtrArray) records = find_related_records (self, klass);

          rtfm_rst_writer_write (writer, ":Inherited:\n   ");

          for (guint i = 0; i < parents->len; i++)
            {
              RtfmGirClass *ancestor = g_ptr_array_index (parents, i);

              if (i != 0)
                rtfm_rst_writer_write (writer, ", ");

              rtfm_rst_writer_write (writer, ":ref:`%s (%u)<%s.methods>`",
                                     rtfm_gir_class_get_c_type (ancestor),
                                     count_methods (ancestor),
                                     rtfm_gir_class_get_c_type (ancestor));
            }

          rtfm_rst_writer_write_line (writer);
          rtfm_rst_writer_write_line (writer);

          if (records->len)
            {
              gboolean found = FALSE;

              for (guint i = 0; i < records->len; i++)
                {
                  RtfmGirRecord *record = g_ptr_array_index (records, i);
                  g_autoptr(GPtrArray) record_methods = CHILDREN_OF (record, RTFM_GIR_TYPE_METHOD);

                  if (record_methods->len)
                    {
                      found = TRUE;
                      break;
                    }
                }

              if (found)
                {
                  rtfm_rst_writer_write (writer, ":Structs:\n");

                  for (guint i = 0; i < records->len; i++)
                    {
                      RtfmGirRecord *record = g_ptr_array_index (records, i);
                      g_autoptr(GPtrArray) record_methods = CHILDREN_OF (record, RTFM_GIR_TYPE_METHOD);

                      if (record_methods->len)
                        rtfm_rst_writer_write (writer, "   :ref:`%s (%u)<%s.methods>`",
                                               rtfm_gir_record_get_c_type (record),
                                               record_methods->len,
                                               rtfm_gir_record_get_c_type (record));
                    }
                }

              rtfm_rst_writer_write_line (writer);
            }

          rtfm_rst_writer_write_line (writer);
        }

      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write (writer, ".. csv-table::\n");
      rtfm_rst_writer_write (writer, "   :widths: 1, 100\n");
      rtfm_rst_writer_write_line (writer);

      for (guint i = 0; i < ctors->len; i++)
        {
          RtfmGirConstructor *ctor = g_ptr_array_index (ctors, i);

          rtfm_gir_file_generate_func_desc_c (self, writer, ctor);
        }

      for (guint i = 0; i < methods->len; i++)
        {
          RtfmGirMethod *method = g_ptr_array_index (methods, i);

          rtfm_gir_file_generate_func_desc_c (self, writer, method);
        }

      for (guint i = 0; i < funcs->len; i++)
        {
          RtfmGirFunction *func = g_ptr_array_index (funcs, i);

          rtfm_gir_file_generate_func_desc_c (self, writer, func);
        }

      rtfm_rst_writer_write_line (writer);
      rtfm_rst_writer_write_line (writer);
    }

  if (virtuals->len)
    {
      rtfm_rst_writer_write (writer, ".. _%s.vfuncs:\n", rtfm_gir_class_get_c_type (klass));
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Virtual Methods");
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write (writer, ".. csv-table::\n");
      rtfm_rst_writer_write (writer, "   :widths: 1, 100\n");
      rtfm_rst_writer_write_line (writer);

      for (guint i = 0; i < virtuals->len; i++)
        {
          RtfmGirVirtualMethod *virtual = g_ptr_array_index (virtuals, i);

          rtfm_gir_file_generate_func_desc_c (self, writer, virtual);
        }

      rtfm_rst_writer_write_line (writer);
      rtfm_rst_writer_write_line (writer);
    }

}

static gpointer
get_ancestor_typed (gpointer instance,
                    GType    type)
{
  for (instance = rtfm_gir_parser_object_get_parent (instance);
       !g_type_is_a (G_OBJECT_TYPE (instance), type);
       instance = rtfm_gir_parser_object_get_parent (instance))
    { }

  return instance;
}

static gpointer
find_type_named (gpointer     tree_item,
                 const gchar *type_name)
{
  RtfmGirNamespace *ns = get_ancestor_typed (tree_item, RTFM_GIR_TYPE_NAMESPACE);
  GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (ns));

  for (guint i = 0; i < children->len; i++)
    {
      gpointer instance = g_ptr_array_index (children, i);
      const gchar *name = NULL;

      if (RTFM_GIR_IS_RECORD (instance))
        name = rtfm_gir_record_get_name (instance);
      else if (RTFM_GIR_IS_CLASS (instance))
        name = rtfm_gir_class_get_name (instance);
      else if (RTFM_GIR_IS_ENUMERATION (instance))
        name = rtfm_gir_enumeration_get_name (instance);
      else if (RTFM_GIR_IS_BITFIELD (instance))
        name = rtfm_gir_bitfield_get_name (instance);

      if (g_strcmp0 (name, type_name) == 0)
        return instance;
    }

  return NULL;
}

static gchar *
get_prop_c_type (RtfmGirProperty *prop)
{
  GPtrArray *children = rtfm_gir_parser_object_get_children (RTFM_GIR_PARSER_OBJECT (prop));

  for (guint i = 0; i < children->len; i++)
    {
      gpointer instance = g_ptr_array_index (children, i);

      if (RTFM_GIR_IS_ARRAY (instance))
        return g_strdup (rtfm_gir_array_get_c_type (instance));
      else if (RTFM_GIR_IS_TYPE (instance))
        {
          const gchar *c_type = rtfm_gir_type_get_c_type (instance);
          gchar *str = NULL;
          gpointer type;

          if (c_type)
            return g_strdup (c_type);

          type = find_type_named (prop, rtfm_gir_type_get_name (instance));

          if (type)
            g_object_get (type, "c-type", &str, NULL);

          return str;
        }
    }

  return NULL;
}

static gchar *
get_prop_access (RtfmGirProperty *prop)
{
  GString *str = g_string_new (NULL);
  const gchar *s;

#define ADD_PART(s) \
  G_STMT_START { \
    if (str->len) \
      g_string_append (str, " / "); \
    g_string_append (str, s); \
  } G_STMT_END

  if (!(s = rtfm_gir_property_get_readable (prop)) || *s == '1')
    ADD_PART ("Read");

  if (rtfm_gir_property_get_writable (prop))
    ADD_PART ("Write");

  if (rtfm_gir_property_get_construct_only (prop))
    ADD_PART ("Construct-Only");
  else if (rtfm_gir_property_get_construct (prop))
    ADD_PART ("Construct");

  return g_string_free (str, FALSE);
}

static void
write_properties_preview (RtfmGirFile   *self,
                          RtfmRstWriter *writer,
                          RtfmGirClass  *klass)
{
  g_autoptr(GPtrArray) props = NULL;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (RTFM_GIR_IS_CLASS (klass));

  props = CHILDREN_OF (klass, RTFM_GIR_TYPE_PROPERTY);

  if (props->len)
    {
      rtfm_rst_writer_write (writer, ".. _%s.props:\n",
                             rtfm_gir_class_get_name (klass));
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Properties");
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write (writer, ".. csv-table::\n");
      rtfm_rst_writer_write (writer, "   :widths: 1, 100, 1\n");
      rtfm_rst_writer_write_line (writer);
    }

  for (guint i = 0; i < props->len; i++)
    {
      RtfmGirProperty *prop = g_ptr_array_index (props, i);
      g_autofree gchar *c_type = get_prop_c_type (prop);
      g_autofree gchar *prop_access = get_prop_access (prop);

      rtfm_rst_writer_write (writer, "        \":c:type:`%s`\",", c_type);
      rtfm_rst_writer_write (writer, "\":ref:`%s <%s.props.%s>`\",",
                             rtfm_gir_property_get_name (prop),
                             rtfm_gir_class_get_c_type (klass),
                             rtfm_gir_property_get_name (prop));
      rtfm_rst_writer_write (writer, "\"%s\"\n", prop_access);
    }

  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);
}

static void
rtfm_gir_file_generate_prop_c (RtfmGirFile     *self,
                               RtfmRstWriter   *writer,
                               RtfmGirClass    *klass,
                               RtfmGirProperty *prop)
{
  g_autofree gchar *c_type = get_prop_c_type (prop);
  g_autofree gchar *prop_access = get_prop_access (prop);
  g_autoptr(RtfmGirDoc) doc = first_child_typed (prop, RTFM_GIR_TYPE_DOC);

  rtfm_rst_writer_write (writer, ".. _%s.props.%s:\n",
                         rtfm_gir_class_get_c_type (klass),
                         rtfm_gir_property_get_name (prop));
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write (writer, ".. c:member:: %s:%s\n",
                         rtfm_gir_class_get_c_type (klass),
                         rtfm_gir_property_get_name (prop));
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write (writer, "   :Name: ``%s``\n",
                         rtfm_gir_property_get_name (prop));
  rtfm_rst_writer_write (writer, "   :Type: :c:type:`%s`\n", c_type);
  rtfm_rst_writer_write (writer, "   :Default Value: \n");
  rtfm_rst_writer_write (writer, "   :Flags: \n");
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);

  if (doc)
    write_text_body_c (writer, rtfm_gir_doc_get_inner_text (doc));

  rtfm_rst_writer_write_line (writer);
}

static void
rtfm_gir_file_generate_class_c (RtfmGirFile   *self,
                                RtfmRstWriter *writer,
                                RtfmGirClass  *klass)
{
  g_autoptr(RtfmGirDoc) cdoc = NULL;
  g_autoptr(GPtrArray) ctors = NULL;
  g_autoptr(GPtrArray) funcs = NULL;
  g_autoptr(GPtrArray) methods = NULL;
  g_autoptr(GPtrArray) vmethods = NULL;
  g_autoptr(GPtrArray) props = NULL;
  const gchar *c_type;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (RTFM_GIR_IS_CLASS (klass));

  c_type = rtfm_gir_class_get_c_type (klass);
  cdoc = first_child_typed (klass, RTFM_GIR_TYPE_DOC);

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H1, "%s", c_type);
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);

  /* Overview of class here */

  write_subclasses (self, writer, klass);
  write_methods_preview (self, writer, klass);
  write_properties_preview (self, writer, klass);

  /* Now class definition, then other groups */

  rtfm_rst_writer_write (writer, ".. c:type:: %s\n", c_type);
  rtfm_rst_writer_write_line (writer);

  if (cdoc)
    write_text_body_c (writer, rtfm_gir_doc_get_inner_text (cdoc));

  if ((ctors = CHILDREN_OF (klass, RTFM_GIR_TYPE_CONSTRUCTOR))->len)
    {
      for (guint i = 0; i < ctors->len; i++)
        rtfm_gir_file_generate_func_c (self, writer, g_ptr_array_index (ctors, i));
    }

  if ((vmethods = CHILDREN_OF (klass, RTFM_GIR_TYPE_VIRTUAL_METHOD))->len)
    {
      for (guint i = 0; i < vmethods->len; i++)
        rtfm_gir_file_generate_func_c (self, writer, g_ptr_array_index (vmethods, i));
    }

  if ((methods = CHILDREN_OF (klass, RTFM_GIR_TYPE_METHOD))->len)
    {
      for (guint i = 0; i < methods->len; i++)
        rtfm_gir_file_generate_func_c (self, writer, g_ptr_array_index (methods, i));
    }

  if ((funcs = CHILDREN_OF (klass, RTFM_GIR_TYPE_FUNCTION))->len)
    {
      for (guint i = 0; i < funcs->len; i++)
        rtfm_gir_file_generate_func_c (self, writer, g_ptr_array_index (funcs, i));
    }

  if ((props = CHILDREN_OF (klass, RTFM_GIR_TYPE_PROPERTY))->len)
    {
      rtfm_rst_writer_write (writer, ".. _%s.props:\n", rtfm_gir_class_get_c_type (klass));
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Property Details");
      rtfm_rst_writer_write_line (writer);

      for (guint i = 0; i < props->len; i++)
        rtfm_gir_file_generate_prop_c (self, writer, klass, g_ptr_array_index (props, i));

      rtfm_rst_writer_write_line (writer);
    }

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Style Properties");
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Signals");
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Fields");
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);
}

static gboolean
rtfm_gir_file_generate_rst_namespace (RtfmGirFile       *self,
                                      RtfmRstWriter     *writer,
                                      RtfmGirRepository *repository,
                                      RtfmGirNamespace  *namespace,
                                      GCancellable      *cancellable,
                                      GError           **error)
{
  g_autoptr(GPtrArray) classes = NULL;
  g_autoptr(GPtrArray) deps = NULL;
  g_autoptr(GPtrArray) funcs = NULL;
  const gchar *name;
  const gchar *version;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (RTFM_GIR_IS_NAMESPACE (namespace));
  g_assert (RTFM_GIR_IS_REPOSITORY (repository));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (!rtfm_rst_writer_push_file (writer, "index.rst", error))
    return FALSE;

  name = rtfm_gir_namespace_get_name (namespace);
  version = rtfm_gir_namespace_get_version (namespace);

  rtfm_rst_writer_write (writer, ".. _%s-%s:\n", name, version);
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H1, "%s %s", name, version);
  rtfm_rst_writer_write_line (writer);
  rtfm_rst_writer_write_line (writer);

  /* Namespace dependencies come from the Repository */
  if ((deps = CHILDREN_OF (repository, RTFM_GIR_TYPE_INCLUDE))->len)
    {
      rtfm_rst_writer_write (writer, ":Dependencies:\n");

      for (guint i = 0; i < deps->len; i++)
        {
          RtfmGirInclude *inc = g_ptr_array_index (deps, i);

          rtfm_rst_writer_write (writer, "   | :ref:`%s %s <%s-%s>`\n",
                                 rtfm_gir_include_get_name (inc),
                                 rtfm_gir_include_get_version (inc),
                                 rtfm_gir_include_get_name (inc),
                                 rtfm_gir_include_get_version (inc));
        }

      rtfm_rst_writer_write_line (writer);
    }

  rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "API");
  rtfm_rst_writer_write_line (writer);

  rtfm_rst_writer_write (writer, ".. toctree::\n");
  rtfm_rst_writer_write (writer, "   :maxdepth: 1\n");
  rtfm_rst_writer_write_line (writer);

  /* If we have classes, we need that page */
  if ((classes = CHILDREN_OF (namespace, RTFM_GIR_TYPE_CLASS))->len)
    {
      rtfm_rst_writer_write (writer, "   classes\n");

      if (!rtfm_rst_writer_push_file (writer, "classes.rst", error))
        return FALSE;

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H1, "Classes");
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write (writer, ".. toctree::\n");
      rtfm_rst_writer_write (writer, "   :maxdepth: 1\n");
      rtfm_rst_writer_write_line (writer);

      for (guint i = 0; i < classes->len; i++)
        {
          RtfmGirClass *klass = g_ptr_array_index (classes, i);
          const gchar *class_name = rtfm_gir_class_get_c_type (klass);
          g_autofree gchar *classfile = g_strdup_printf ("%s.rst", class_name);

          /* GdkPixbuf hits here with private succlasses */
          if (str_empty0 (class_name))
            continue;

          rtfm_rst_writer_write (writer, "   classes/%s\n", class_name);

          if (!rtfm_rst_writer_push_directory (writer, "classes", error) ||
              !rtfm_rst_writer_push_file (writer, classfile, error))
            return FALSE;

          rtfm_gir_file_generate_class_c (self, writer, klass);

          rtfm_rst_writer_pop_file (writer);
          rtfm_rst_writer_pop_directory (writer);
        }

      rtfm_rst_writer_pop_file (writer);
    }

  /* If we have functions at the namespace level, we need that page */
  if ((funcs = CHILDREN_OF (namespace, RTFM_GIR_TYPE_FUNCTION))->len)
    {
      rtfm_rst_writer_write (writer, "   functions\n");

      if (!rtfm_rst_writer_push_file (writer, "functions.rst", error))
        return FALSE;

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H1, "Functions");
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write (writer, ".. csv-table::\n");
      rtfm_rst_writer_write (writer, "   :widths: 1, 100\n");
      rtfm_rst_writer_write_line (writer);
      for (guint i = 0; i < funcs->len; i++)
        rtfm_gir_file_generate_func_desc_c (self, writer, g_ptr_array_index (funcs, i));
      rtfm_rst_writer_write_line (writer);
      rtfm_rst_writer_write_line (writer);

      rtfm_rst_writer_write_header (writer, RTFM_RST_HEADER_H2, "Details");
      rtfm_rst_writer_write_line (writer);

      for (guint i = 0; i < funcs->len; i++)
        rtfm_gir_file_generate_func_c (self, writer, g_ptr_array_index (funcs, i));

      rtfm_rst_writer_pop_file (writer);
    }

  rtfm_rst_writer_pop_file (writer);

  return TRUE;
}

gboolean
rtfm_gir_file_generate_rst (RtfmGirFile    *self,
                            RtfmRstWriter  *writer,
                            GCancellable   *cancellable,
                            GError        **error)
{
  g_autofree gchar *path = NULL;
  g_autoptr(GPtrArray) children = NULL;
  RtfmGirRepository *repository;
  GFile *file;

  g_assert (RTFM_GIR_IS_FILE (self));
  g_assert (RTFM_IS_RST_WRITER (writer));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  repository = rtfm_gir_file_get_repository (self);
  file = rtfm_gir_file_get_file (self);

  if (repository == NULL &&
      !g_initable_init (G_INITABLE (self), cancellable, error))
    return FALSE;

  repository = rtfm_gir_file_get_repository (self);
  g_assert (RTFM_GIR_IS_REPOSITORY (repository));

  path = g_file_get_basename (file);
  g_message ("Generating RST for %s", path);

  children = CHILDREN_OF (repository, RTFM_GIR_TYPE_NAMESPACE);

  for (guint i = 0; i < children->len; i++)
    {
      RtfmGirNamespace *child = g_ptr_array_index (children, i);
      const gchar *name = rtfm_gir_namespace_get_name (child);
      const gchar *version = rtfm_gir_namespace_get_version (child);
      g_autofree gchar *fullname = g_strdup_printf ("%s-%s", name, version);

      g_message (" Found namespace %s with verison %s", name, version);

      if (!rtfm_rst_writer_push_directory (writer, fullname, error))
        return FALSE;

      if (!rtfm_gir_file_generate_rst_namespace (self, writer, repository, child, cancellable, error))
        return FALSE;

      rtfm_rst_writer_pop_directory (writer);
    }

  return TRUE;
}
